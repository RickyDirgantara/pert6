import transportasi.Kendaraan;
import transportasi.Sepeda;

public class TransportasiDemo {
    public static void main(String[] args) {
        // membuat objek Kendaraan
        Kendaraan mobil = new Kendaraan();
        Sepeda sepeda = new Sepeda();
        mobil.berjalan("Maju");
        mobil.berjalan("Mundur");
        mobil.berjalan("Belok");
        mobil.berhenti();
        mobil.roda = 4;
        mobil.infoKendaraan();
        sepeda.bmx();
        sepeda.ngerem();
    }
}
